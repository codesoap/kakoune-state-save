hook global KakBegin .* %{ require-module state-save }
hook global ModuleLoaded state-save %{
    state-save-reg-load dquote
    state-save-reg-load slash
    state-save-reg-load colon
    state-save-reg-load arobase
    state-save-reg-load caret
    state-save-reg-load pipe
}
hook global KakEnd .* %{
    state-save-reg-save dquote
    state-save-reg-save slash
    state-save-reg-save colon
    state-save-reg-save arobase
    state-save-reg-save caret
    state-save-reg-save pipe
}

provide-module state-save %{

declare-option -docstring "Where the state-save plugin saves buffer state" \
str state_save_path \
%sh{
    printf "%s\n" "${XDG_DATA_HOME:-$HOME/.local/share/}/kak/state-save/"
}

define-command state-save-reg-load -params 1 -docstring "
state-save-reg-load <regname>

Load the named register from disk.
The colon, slash, and pipe registers will merge the new contents rather
than replacing the old." \
%{
    evaluate-commands %sh{
        if [ -f "$kak_opt_state_save_path/$1.register" ]; then
            printf "%s\n" "eval reg $1 %file{$kak_opt_state_save_path/$1.register}"
        fi
    }
}

define-command state-save-reg-save -params 1 -docstring "
state-save-reg-save <regname>

Store the named register to disk." \
%{
    nop %sh{ mkdir -p "$kak_opt_state_save_path" }
    evaluate-commands echo \
        -to-file "%opt{state_save_path}/%arg{1}.register" \
        -quoting kakoune \
        "%%reg{%arg{1}}"
}

} # End of module "state-save".
